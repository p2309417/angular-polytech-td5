import { Component, EventEmitter, LOCALE_ID, Output, ViewEncapsulation } from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { AEROPORTS } from './../../constants/aeroport.constant';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { IAeroport } from '../../models/aeroport.model';
import { ThreeDayRangeSelectionStrategy } from '../../date-adapter';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import {MatCommonModule} from '@angular/material/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormControl,ReactiveFormsModule } from '@angular/forms';
import { Filter } from '../../models/filter.model';


@Component({
  selector: 'app-filtres',
  standalone: true,
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  imports: [MatIconModule, MatButtonModule,ReactiveFormsModule, MatInputModule,MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatCommonModule, CommonModule],
  providers: [
    provideNativeDateAdapter(),
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: ThreeDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */

  @Output() filterChange = new EventEmitter<Filter>();

  aeroports: IAeroport[] = AEROPORTS;
  aeroport = new FormControl(null);
  dateDebut = new FormControl(null);
  dateFin = new FormControl(null);

  handleClick(): void {
    if(this.aeroport.value != null && this.dateDebut.value != null && this.dateFin.value != null){
      console.log(this.dateDebut.value)
      this.filterChange.emit({
        aeroport: this.aeroport.value,
        dateDebut: (new Date(this.dateDebut.value)).getTime(),
        dateFin: (new Date(this.dateFin.value)).getTime()
      });
    }
  }

}
