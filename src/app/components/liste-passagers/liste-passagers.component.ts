import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { PassagerComponent } from '../passager/passager.component';
import { CommonModule } from '@angular/common';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { FormControl,ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';


@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [PassagerComponent,CommonModule,MatSlideToggleModule,MatFormFieldModule,ReactiveFormsModule],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  @Input() passagers!: Passager[];
  displayPhotos= new FormControl();

  handleToogle() : void {
    console.log(this.displayPhotos.value);
  }

}
