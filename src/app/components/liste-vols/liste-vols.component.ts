import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { VolComponent } from '../vol/vol.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [VolComponent,CommonModule],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {

  @Input() vols!: Vol[];
  @Input() type!: string;
  @Output() emitVol = new EventEmitter<Vol>();

  
  constructor(){console.log(this.vols)}

  handleClick(vol:Vol) :void {
    console.log('vol clicked')
    this.emitVol.emit(vol);
  }
}
