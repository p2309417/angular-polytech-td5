import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatIcon } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { ClassVolDirective } from '../../directives/class-vol.directive';
import { PoidsVolDirective } from '../../directives/poids-vol.directive';
import { MatTooltipModule} from '@angular/material/tooltip';


@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [MatIcon,CommonModule, ClassVolDirective, PoidsVolDirective,MatTooltipModule],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input() passager!: Passager;
  @Input() displayPhoto: boolean = false;
}
