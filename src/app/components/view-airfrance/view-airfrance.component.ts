import { Component } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IAeroport } from '../../models/aeroport.model';
import { Filter } from '../../models/filter.model';
import { VolService } from '../../services/vol.service';
import { IVol, Vol } from '../../models/vol.model';
import { PassagerService } from '../../services/passager.service';
import { Passager } from '../../models/passager.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent {

  filter!: Filter;
  vols!: Vol[];
  passagers!: Passager[];
  type!: string;

  constructor(private volService:VolService,private passagerService: PassagerService, private activatedRoute: ActivatedRoute){
    activatedRoute.url.subscribe((url) => {
      this.type = url[0].path;
    });
  }

  handleFilterChange(filter:Filter):void {
    this.filter = filter;
    if(this.type === 'atterrissages'){
      console.log('atterrissages')
      this.volService.getVolsArrivee(this.filter.aeroport.icao,this.filter.dateDebut, this.filter.dateFin).subscribe((vols:Vol[]) => {
        this.vols = vols;
        console.log(vols);
      });
    }else if(this.type === 'decollages'){
      this.volService.getVolsDepart(this.filter.aeroport.icao,this.filter.dateDebut, this.filter.dateFin).subscribe((vols:Vol[]) => {
        this.vols = vols;
      });
    }
  }

  handleEmitVol(vol:Vol) :void {
    console.log(vol);
    this.passagerService.getVolsPassagers(vol.icao).subscribe((pass: Passager[]) => {
      this.passagers = pass; 
      console.log(pass)
    });
  }

}
