import { Component, Input } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { MatIcon } from '@angular/material/icon';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [MatIcon,CommonModule],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() vol!: Vol;
  @Input() type!: string;
  

}
