import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appClassVol]',
  standalone: true
})
export class ClassVolDirective implements OnInit{

  @Input() classVol!: string;
  constructor(private elementRef: ElementRef) {}
  ngOnInit(): void {
    if(this.classVol === 'BUSINESS') this.elementRef.nativeElement.style.color = 'red'
    if(this.classVol === 'STANDARD') this.elementRef.nativeElement.style.color = 'blue'
    if(this.classVol === 'PREMIUM') this.elementRef.nativeElement.style.color = 'green'
  }

}
