import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appPoidsVol]',
  standalone: true
})
export class PoidsVolDirective implements OnInit{

  @Input() classVol!: string;
  @Input() nbLuggage!: number;
  constructor(private elementRef: ElementRef) { }
  ngOnInit(): void {
    if(
      (this.classVol === 'STANDARD' && this.nbLuggage > 1) ||
      (this.classVol === 'BUSINESS' && this.nbLuggage > 2) ||
      (this.classVol === 'PREMIUM' && this.nbLuggage > 3) 
    ){
      this.elementRef.nativeElement.style.backgroundColor = 'red';
    }
  }

}
