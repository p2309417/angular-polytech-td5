import { IAeroport } from "./aeroport.model";

export interface Filter {
    aeroport: IAeroport,
    dateDebut: number,
    dateFin: number
  }
  