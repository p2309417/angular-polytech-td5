import { Injectable } from '@angular/core';
import { IPassagerDto, Passager } from '../models/passager.model';
import { Observable, map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor(private http: HttpClient) { }

  getVolsPassagers(code: string): Observable<Passager[]> {
    return this.http.get<any>(`https://randomuser.me/api/?results=20&inc=name,picture,email&seed=${code}`).pipe(
      map((response) => response.results
      .map((passager: IPassagerDto) => new Passager(passager))
    )
    );
  }

}
